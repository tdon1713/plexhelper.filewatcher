﻿using PlexHelper.FileWatcher.WinForm.Forms;
using PlexHelper.FileWatcher.WinForm.Utility;
using System;
using System.IO;
using System.Windows.Forms;

namespace PlexHelper.FileWatcher.WinForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!Directory.Exists(Constants.GetAppDataFolder()))
            {
                Directory.CreateDirectory(Constants.GetAppDataFolder());
            }

            Globals globals = Globals.GetInstance();
            UserSettings curSettings = globals.Settings;

            frmMain main = new frmMain
            {
                LaunchSettings = globals.LaunchSettings
            };

            Application.Run(main);
        }
    }
}
