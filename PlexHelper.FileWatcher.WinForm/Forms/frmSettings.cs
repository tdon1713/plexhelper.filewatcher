﻿using PlexHelper.FileWatcher.WinForm.Utility;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace PlexHelper.FileWatcher.WinForm.Forms
{
    public partial class frmSettings : Form
    {
        private Globals _globals;

        #region Debug Menu Events

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        public Action OnClose { get; set; } = null;

        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            _globals = Globals.GetInstance();
#if DEBUG
            menuStrip1.Visible = true;
#endif
        }

        private void frmSettings_Shown(object sender, EventArgs e)
        {
            txtDefaultDirectory.Text = _globals.Settings.DefaultMoveDirectory;

            foreach (string item in _globals.Settings.DirectoriesToWatch)
            {
                lstWatchingDirectories.Items.Add(item);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _globals.Settings.DefaultMoveDirectory = txtDefaultDirectory.Text.Trim();
            _globals.Settings.DirectoriesToWatch = lstWatchingDirectories.Items.Cast<string>().Select(x => x).ToList();
            _globals.Settings.Save();

            Enabled = false;
            OnClose?.Invoke();
            Close();
        }

        private void btnAddDirectory_Click(object sender, EventArgs e)
        {
            using (var folderDialog = new FolderBrowserDialog())
            {
                DialogResult result = folderDialog.ShowDialog();

                if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(folderDialog.SelectedPath))
                {
                    if (!lstWatchingDirectories.Items.Contains(folderDialog.SelectedPath))
                    {
                        lstWatchingDirectories.Items.Add(folderDialog.SelectedPath);
                    }
                }
            }
        }

        private void btnRemoveDirectory_Click(object sender, EventArgs e)
        {
            if (lstWatchingDirectories.SelectedIndex < 0)
            {
                return;
            }

            lstWatchingDirectories.Items.RemoveAt(lstWatchingDirectories.SelectedIndex);
        }

        private void btnSelectDefaultDirectory_Click(object sender, EventArgs e)
        {
            using (var folderDialog = new FolderBrowserDialog())
            {
                DialogResult result = folderDialog.ShowDialog();

                if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(folderDialog.SelectedPath))
                {
                    if (!lstWatchingDirectories.Items.Contains(folderDialog.SelectedPath))
                    {
                        txtDefaultDirectory.Text = folderDialog.SelectedPath;
                    }
                }
            }
        }
    }
}
