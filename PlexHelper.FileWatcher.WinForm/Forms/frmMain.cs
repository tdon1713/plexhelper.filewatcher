﻿using PlexHelper.FileWatcher.WinForm.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PlexHelper.FileWatcher.WinForm.Utility.Enumerations;

namespace PlexHelper.FileWatcher.WinForm.Forms
{
    public partial class frmMain : Form
    {
        #region Private Variables

        private bool _isExecutingRenameAndMove = false;
        private List<string> _newFilesDuringExecution = new List<string>();
        private Globals _globals;
        private List<FileSystemWatcher> _watchers = new List<FileSystemWatcher>();

        #endregion

        #region Public Properties

        public bool LaunchSettings { get; set; }

        #endregion

        public frmMain()
        {
            InitializeComponent();
        }

        #region Form Events

        private void frmMain_Load(object sender, EventArgs e)
        {
            _globals = Globals.GetInstance();
            if (LaunchSettings)
            {
                return;
            }

            SetupWatchers();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            if (LaunchSettings)
            {
                LaunchSettings = false;
                FormHandler.ShowSettings(() =>
                {
                    SetupWatchers();
                });
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (FileSystemWatcher watcher in _watchers)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Created -= OnFileCreated;
                watcher.Dispose();
            }

            _watchers.Clear();
            _watchers = null;
        }

        #endregion

        #region DataGrid Events
        
        private void dgvFiles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == (int)FileWatcherGridColumns.NewLocation)
            {
                using (var folderDialog = new FolderBrowserDialog())
                {
                    if (!String.IsNullOrWhiteSpace(_globals.Settings.DefaultMoveDirectory) && Directory.Exists(_globals.Settings.DefaultMoveDirectory))
                    {
                        folderDialog.SelectedPath = _globals.Settings.DefaultMoveDirectory;
                    }

                    DialogResult result = folderDialog.ShowDialog();
                    if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(folderDialog.SelectedPath))
                    {
                        dgvFiles[e.ColumnIndex, e.RowIndex].Value = folderDialog.SelectedPath;
                        dgvFiles.EndEdit();
                        dgvFiles.Refresh();
                    }
                }
            }
        }

        #endregion

        #region Button events

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (dgvFiles.Rows.Count == 0)
            {
                return;
            }

            _isExecutingRenameAndMove = true;

            dgvFiles.Enabled = false;
            btnExecute.Enabled = false;

            lstStatus.Items.Clear();

            List<DataGridViewRow> rowsToRemove = new List<DataGridViewRow>();

            Task.Run(async () =>
            {
                foreach (DataGridViewRow row in dgvFiles.Rows)
                {
                    string fileName = row.Cells[(int)FileWatcherGridColumns.FileName].Value.ToString();

                    var season = row.Cells[(int)FileWatcherGridColumns.Season].Value;
                    if (season == null || !Int32.TryParse(season.ToString(), out int seasonResult))
                    {
                        AddMessage($"Failed on file {fileName}; Season not supplied or is not a number");
                        continue;
                    }

                    var episode = row.Cells[(int)FileWatcherGridColumns.Episode].Value;
                    if (episode == null || !Int32.TryParse(episode.ToString(), out int episodeResult))
                    {
                        AddMessage($"Failed on file {fileName}; Episode not supplied or is not a number");
                        continue;
                    }

                    var newLocation = row.Cells[(int)FileWatcherGridColumns.NewLocation].Value;
                    if (newLocation != null && !newLocation.ToString().EndsWith("\\"))
                    {
                        newLocation = newLocation.ToString() + "\\";
                    }

                    if (newLocation == null || !Directory.Exists(newLocation.ToString()))
                    {
                        AddMessage($"File {fileName} will be renamed, but will not be moved.");
                    }

                    FileInfo info = new FileInfo(fileName);
                    string directory = info.DirectoryName;
                    if (newLocation != null)
                    {
                        directory = newLocation.ToString();
                    }

                    string newFileName = $@"{directory}\S{seasonResult.ToString().PadLeft(2, '0')}E{episodeResult.ToString().PadLeft(2, '0')} {info.Name}";
                    AddMessage($"Beginning Renaming/Moving {fileName}");

                    try
                    {
                        File.Move(fileName, newFileName);
                        AddMessage($"Successfully renamed/moved {newFileName}");
                        rowsToRemove.Add(row);
                    }
                    catch
                    {
                        AddMessage($"Unexpected error renaming/moving {fileName}");
                    }
                }
            }).ContinueWith((result) =>
            {
                foreach (DataGridViewRow row in rowsToRemove)
                {
                    dgvFiles.Rows.Remove(row);
                }
                rowsToRemove.Clear();

                foreach (string newFile in _newFilesDuringExecution)
                {
                    AddFileToGrid(newFile);
                }
                _newFilesDuringExecution.Clear();

                _isExecutingRenameAndMove = false;

                btnExecute.Enabled = true;
                dgvFiles.Enabled = true;

                Thread.Sleep(0);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion

        #region Custom Events

        private void OnFileCreated(object source, FileSystemEventArgs e)
        {
            if (File.Exists(e.FullPath))
            {
                if (_isExecutingRenameAndMove)
                {
                    _newFilesDuringExecution.Add(e.FullPath);
                    return;
                }

                AddFileToGrid(e.FullPath);
            }
        }

        #endregion

        #region Custom Methods

        private void AddFileToGrid(string path)
        {
            dgvFiles.Invoke((MethodInvoker)delegate
            {
                int row = dgvFiles.Rows.Add();
                dgvFiles.Rows[row].Cells[(int)FileWatcherGridColumns.FileName].Value = path;
            });
        }

        private void AddMessage(string message)
        {
            lstStatus.Invoke((MethodInvoker)delegate
            {
                lstStatus.Items.Add(message);
                lstStatus.SetSelected(lstStatus.Items.Count - 1, true);
            });
        }

        private void SetupWatchers()
        {
            foreach (string path in _globals.Settings.DirectoriesToWatch)
            {
                if (_watchers.Any(x => x.Path == path))
                {
                    continue;
                }

                if (!Directory.Exists(path))
                {
                    continue;
                }

                FileSystemWatcher _watcher = new FileSystemWatcher
                {
                    Path = path,
                    NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                    | NotifyFilters.FileName | NotifyFilters.DirectoryName,
                    Filter = "*.*"
                };
                _watcher.Created += new FileSystemEventHandler(OnFileCreated);
                _watcher.EnableRaisingEvents = true;

                _watchers.Add(_watcher);
            }
        }

        #endregion

        #region Menu Events

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormHandler.ShowSettings();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
            Application.Exit();
        }

        private void loadFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvFiles.Rows.Clear();
            dgvFiles.DataSource = null;
            dgvFiles.Refresh();

            foreach (string directory in _globals.Settings.DirectoriesToWatch)
            {
                if (!Directory.Exists(directory))
                {
                    continue;
                }

                string[] files = Directory.GetFiles(directory);
                foreach (string file in files)
                {
                    AddFileToGrid(file);
                }
            }

            dgvFiles.Refresh();
        }

        #endregion

        
    }
}
