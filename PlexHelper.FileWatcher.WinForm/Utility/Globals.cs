﻿using Newtonsoft.Json;

namespace PlexHelper.FileWatcher.WinForm.Utility
{
    internal sealed class Globals
    {
        #region Instancing

        private static readonly object _lock = new object();
        private static Globals _inst;

        internal static Globals GetInstance()
        {
            lock (_lock)
            {
                if (_inst == null)
                {
                    _inst = new Globals();
                }
            }

            return _inst;
        }

        #endregion

        #region Private Variables

        private UserSettings _settings = null;

        #endregion

        public UserSettings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new UserSettings();
                    if (!System.IO.File.Exists(Constants.GetSettingsPath()))
                    {
                        LaunchSettings = true;
                        _settings.Save();
                    }
                    else
                    {
                        _settings = JsonConvert.DeserializeObject<UserSettings>(System.IO.File.ReadAllText(Constants.GetSettingsPath()));
                    }
                }

                return _settings;
            }
            set { _settings = value; }
        }

        public bool LaunchSettings { get; private set; }
    }
}
