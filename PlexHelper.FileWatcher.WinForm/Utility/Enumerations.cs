﻿namespace PlexHelper.FileWatcher.WinForm.Utility
{
    public class Enumerations
    {
        public enum FileWatcherGridColumns
        {
            FileName = 0,
            Season = 1,
            Episode = 2,
            NewLocation = 3
        }
    }
}
