﻿using Newtonsoft.Json;
using System.IO;

namespace PlexHelper.FileWatcher.WinForm.Utility
{
    public static class Extensions
    {
        public static void Save(this UserSettings source)
        {
            string json = JsonConvert.SerializeObject(source, Formatting.Indented);
            File.WriteAllText(Constants.GetSettingsPath(), json);
        }
    }
}
