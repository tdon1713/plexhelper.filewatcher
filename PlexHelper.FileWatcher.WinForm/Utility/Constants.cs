﻿using System;

namespace PlexHelper.FileWatcher.WinForm.Utility
{
    public static class Constants
    {
        public const string SettingsFileName = "user_settings.json";

        public static string GetAppDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\onyxonstudios\filewatcher (ph)\";
        }

        public static string GetSettingsPath() // C:\Users\tyler\AppData\Roaming\onyxonstudios\filewatcher (ph)
        {
            return GetAppDataFolder() + SettingsFileName;
        }
    }
}
