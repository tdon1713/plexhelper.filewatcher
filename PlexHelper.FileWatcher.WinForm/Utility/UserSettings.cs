﻿using System.Collections.Generic;

namespace PlexHelper.FileWatcher.WinForm.Utility
{
    public class UserSettings
    {
        public string DefaultMoveDirectory { get; set; } = string.Empty;

        public List<string> DirectoriesToWatch { get; set; } = new List<string>();
    }
}
