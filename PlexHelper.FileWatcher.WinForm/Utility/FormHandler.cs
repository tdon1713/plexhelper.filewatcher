﻿using PlexHelper.FileWatcher.WinForm.Forms;
using System;

namespace PlexHelper.FileWatcher.WinForm.Utility
{
    public static class FormHandler
    {
        public static void ShowSettings(Action action = null)
        {
            frmSettings settings = new frmSettings
            {
                OnClose = action
            };
            settings.ShowDialog();
        }
    }
}
