﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlexHelper.FileWatcher.WinForm.ViewModels
{
    public class WatchedFileViewModel
    {
        public string File { get; set; }
    }
}
